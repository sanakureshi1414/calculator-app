package com.example.assign3_calculator;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.GridLayout;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<ButtonData> buttonData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // call function to create layout with buttons
        initializeData();

        // create main grid layout
        GridLayout mainLayout = new GridLayout(this);
        mainLayout.setBackgroundColor(getColor(R.color.black));
        mainLayout.setColumnCount(4);

        // create calculator display
        CalculatorExpressionDisplay display = new CalculatorExpressionDisplay(this);
        mainLayout.addView(display);

        for (ButtonData data : buttonData){
            CalculatorButton button = new CalculatorButton(this, data);
            button.setOnClickListener((view) -> {
                if(data.text.equals("=")){
                    // do calculation thing here
                    String expression = display.getCalculatorInput();
                    double result = Calculator.evaluate(expression);
                    display.setCalculatorInput(result + "");
                } else if (data.text.equals("C")){
                    display.setCalculatorInput("");
                } else {
                    display.setCalculatorInput(display.getCalculatorInput() + data.text);
                }
            });
            mainLayout.addView(button);
        }
        setContentView(mainLayout);
    }

    private  void initializeData() {
        buttonData = new ArrayList<ButtonData>(){
            {
                add(new ButtonData("7", 1, 0, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData("8", 1, 1, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData("9", 1, 2, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData(" / ", 1, 3, 1, getColor(R.color.operations_background)));
                add(new ButtonData("4", 2, 0, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData("5", 2, 1, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData("6", 2, 2, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData(" * ", 2, 3, 1, getColor(R.color.operations_background)));
                add(new ButtonData("1", 3, 0, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData("2", 3, 1, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData("3", 3, 2, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData(" - ", 3, 3, 1, getColor(R.color.operations_background)));
                add(new ButtonData("0", 4, 0, 2, getColor(R.color.numberedButton_background)));
                add(new ButtonData(".", 4, 2, 1, getColor(R.color.numberedButton_background)));
                add(new ButtonData(" + ", 4, 3, 1, getColor(R.color.operations_background)));
                add(new ButtonData("=", 5, 0, 4, getColor(R.color.operations_background)));
                add(new ButtonData("C", 0, 3, 1, getColor(R.color.operations_background)));
            }
        };
    }
}
