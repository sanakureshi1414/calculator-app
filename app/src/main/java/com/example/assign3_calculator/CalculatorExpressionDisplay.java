package com.example.assign3_calculator;

import android.content.Context;
import android.view.Gravity;
import android.widget.GridLayout;
import androidx.appcompat.widget.AppCompatTextView;

public class CalculatorExpressionDisplay extends AppCompatTextView {
    String calculatorInput = "";

    public CalculatorExpressionDisplay(Context context){
        super(context);
        setText(calculatorInput);
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.rowSpec = GridLayout.spec(0, 1, 1);
        params.columnSpec = GridLayout.spec(0, 2, 1);
        setGravity(Gravity.NO_GRAVITY);

        // text display on screen
        setTextColor(getResources().getColor(R.color.white, null));
        setTextSize(18);

        setLayoutParams(params);
    }

    public void setCalculatorInput(String calculatorInput){
        this.calculatorInput = calculatorInput;
        setText(calculatorInput);
    }

    public String getCalculatorInput(){ return calculatorInput; }
}
