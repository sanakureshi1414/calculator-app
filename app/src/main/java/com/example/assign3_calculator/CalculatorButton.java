package com.example.assign3_calculator;

import android.content.Context;
import android.widget.GridLayout;
import androidx.appcompat.widget.AppCompatButton;

public class CalculatorButton extends AppCompatButton {
    public CalculatorButton(Context context, ButtonData data){
        super(context);
        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.rowSpec = GridLayout.spec(data.row, 1, 1);
        params.columnSpec = GridLayout.spec(data.col, data.colSpan, 1);
        params.setMargins(3, 3, 3, 3);
        setLayoutParams(params);
        if (data.color != null){
            setBackgroundColor(data.color);
        } else {
            setBackgroundColor(getResources().getColor(R.color.teal_700));
        }
        setTextSize(24);
        setTextColor(getResources().getColor(R.color.white, null));
        setText(data.text);
    }
}

